package com.gkd.sourceleveldebugger;

import hk.quantr.javalib.swing.advancedswing.enhancedtextarea.EnhancedTextArea;
import java.awt.BorderLayout;
import javax.swing.JFrame;

public class SourceDialog extends javax.swing.JDialog {
	public EnhancedTextArea enhancedTextArea1;

	public SourceDialog(JFrame frame, String title) {
		super(frame, title, true);
		try {
			{
				enhancedTextArea1 = new EnhancedTextArea();
				getContentPane().add(enhancedTextArea1, BorderLayout.CENTER);
				enhancedTextArea1.setText("enhancedTextArea1");
			}
			this.setSize(784, 570);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
