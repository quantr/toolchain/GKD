package com.gkd;

import java.io.IOException;
import java.io.InputStream;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestQemu {

	@Test
	public void test() throws IOException, InterruptedException {
		Process p = Runtime.getRuntime().exec("/usr/local/bin/qemu-system-riscv64 -machine virt -bios none -kernel kernel -m 128M -smp 3 -nographic -drive file=fs.img,if=none,format=raw,id=x0 -device virtio-blk-device,drive=x0,bus=virtio-mmio-bus.0 --trace events=/tmp/events -S -gdb tcp::1234 -chardev socket,id=qmp,host=0.0.0.0,port=4444 -mon chardev=qmp,mode=control");
		InputStream qemuInputStream = p.getInputStream();
		InputStream qemuErrorStream = p.getErrorStream();
		new Thread("Qemu stub") {
			public void run() {
				try {
					int x;
					while ((x = qemuInputStream.read()) != -1) {
						System.out.print((char) x);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}.start();
		new Thread("Qemu stub") {
			public void run() {
				try {
					int x;
					while ((x = qemuErrorStream.read()) != -1) {
						System.err.print((char) x);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}.start();
		p.waitFor();
	}

}
