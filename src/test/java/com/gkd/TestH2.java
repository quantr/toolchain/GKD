package com.gkd;

import com.gkd.hibernate.HibernateUtil;
import com.gkd.instrument.callgraph.JmpData;
import java.util.Date;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestH2 {

    @Test
    public void test() throws ClassNotFoundException {
        Session session = HibernateUtil.openSession();
        Class.forName("org.h2.Driver");
        Transaction tx = session.beginTransaction();
        JmpData jmpData = new JmpData(0, new Date(), (byte) -1, (byte) -1, 1, "1", 5, null, "4", 10, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, null, null, true, null, 1);
        session.save(jmpData);
        tx.commit();
        session.close();
    }
}
